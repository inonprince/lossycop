#!/usr/bin/env python

import argparse
import os
from time import time

import numpy as np
import soundfile as sf
from tensorflow.keras import backend as K
from tensorflow.keras import callbacks
from tensorflow.keras.utils import Sequence, to_categorical

from lossycop.common import BASE_DIR, CLASS_NAMES, BATCH_SIZE, get_model, get_rms, RMS_THRESHOLD, CLASS_QUIET


class LossyCopTensorBoard(callbacks.TensorBoard):
    def on_epoch_end(self, epoch, logs=None):
        logs.update({'lr': K.eval(self.model.optimizer.lr)})
        super().on_epoch_end(epoch, logs)


class SourceSequence(Sequence):
    def __init__(self, path, batchsize):
        self.path = path
        self.batchsize = batchsize

        self.files = []
        for class_name in CLASS_NAMES:
            for example in os.listdir(os.path.join(path, class_name)):
                self.files.append(os.path.join(class_name, example))

    def __len__(self):
        return len(self.files) // self.batchsize

    def __getitem__(self, idx):
        start_i = idx * self.batchsize
        end_i = (idx + 1) * self.batchsize

        self.data_x = []
        self.data_y = []
        for source_file in self.files[start_i:end_i]:
            data, _ = sf.read(os.path.join(self.path, source_file))

            if get_rms(data) < RMS_THRESHOLD:
                class_idx = CLASS_QUIET
            else:
                class_idx = CLASS_NAMES.index(source_file.split('/')[0])

            self.data_x.append(data)
            self.data_y.append(to_categorical(class_idx, 3))

        return np.array(self.data_x), np.array(self.data_y)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', default=20, type=int)
    parser.add_argument('dataset_path')
    args = parser.parse_args()

    train_dir = os.path.join(args.dataset_path, 'train')
    validation_dir = os.path.join(args.dataset_path, 'validation')

    train_generator = SourceSequence(train_dir, BATCH_SIZE)
    validation_generator = SourceSequence(validation_dir, BATCH_SIZE)
    print('Training samples:', len(train_generator))
    print('Validation samples: ', len(validation_generator))

    tensorboard = LossyCopTensorBoard(
        log_dir=os.path.join(BASE_DIR, 'logs', str(int(time()))),
        update_freq='batch',
    )

    checkpoint = callbacks.ModelCheckpoint(
        filepath=os.path.join(BASE_DIR, 'weights.{epoch:02d}-{val_loss:.4f}-{val_acc:.4f}.h5'),
        monitor='val_loss',
        verbose=1,
        save_best_only=True,
        save_weights_only=True,
    )

    model = get_model()
    model.summary()
    history = model.fit_generator(
        train_generator,
        epochs=args.epochs,
        validation_data=validation_generator,
        verbose=1,
        callbacks=[
            tensorboard,
            checkpoint,
        ],
    )


if __name__ == '__main__':
    main()

# data, samplerate = sf.read('file.flac', frames=22050, start=44100 * 2)
# t_input = np.array([data])
# print(t_input.shape)

# tf_samples = tf.placeholder(shape=(1, 22050, 2), dtype=tf.float64)
# res = sess.run(preprocess_step(tf_samples), feed_dict={
#     tf_samples: t_input,
# })
# print(res.shape)
# plt.pcolormesh(np.transpose(res[0, :, 0]), cmap="magma")
